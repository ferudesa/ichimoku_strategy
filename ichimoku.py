import matplotlib.pyplot as plt
import numpy as np
from yfinance.base import TickerBase, utils


class Ichimoku(TickerBase):
    def __init__(self, ticker):
        super().__init__(ticker)
        self._position = None

    def __repr__(self) -> str:
        return f'Ichimoku object <{self.ticker}>'

    def history(self, period="max", interval="1d",
                start=None, end=None):
        """Return the price history with the Ichimoku values.

        Args:
            period (str, optional):
                Valid periods: 1d,5d,1mo,3mo,6mo,1y,2y,5y,10y,ytd,max.
                Either Use period parameter or use start and end.
                Defaults to "max".
            interval (str, optional):
                Valid intervals: 1m,2m,5m,15m,30m,60m,90m,1h,1d,5d,1wk,1mo,3mo.
                Intraday data cannot extend last 60 days.
                Defaults to "1d".
            start (str, optional):
                Download start date string (YYYY-MM-DD) or _datetime.
                Defaults to 1900-01-01.
            end (str, optional):
                Download end date string (YYYY-MM-DD) or _datetime.
                Defaults to now.

        Returns:
            DataFrame: Price history with the Ichimoku values.
        """
        super().history(period=period, interval=interval,
                        start=start, end=end)
        try:
            df = self._history[['Close', 'High', 'Low']].copy()
            df.dropna(inplace=True)

            df['Conversion Line'] = self._period_average(9)
            df['Base Line'] = self._period_average(26)
            df['Span A'] = self._average(df['Conversion Line'],
                                         df['Base Line']).shift(26)
            df['Span B'] = self._period_average(52).shift(26)
            df['Lagging Span'] = df['Close'].shift(-26)

            df.drop(columns=['High', 'Low'], inplace=True)
            df.dropna(subset=['Span B', 'Span A'], inplace=True)
            self._history = df.copy()
            return df
        except Exception:
            pass
        return utils.empty_df()

    def _average(self, first_value, second_value):
        return (first_value + second_value) / 2

    def _period_average(self, period):
        period_high = self._history['High'].rolling(period).max()
        period_low = self._history['Low'].rolling(period).min()
        return self._average(period_high, period_low)

    def _strategy(self):
        """Return the historical recommended positions following
           an Ichimoku strategy

        Returns:
            DataFrame:
                 1 = LONG
                 0 = WAIT
                -1 = SHORT
        """
        if self._history is None:
            self.history()
        try:
            df = self._history.copy()
            # Open Long Position
            df['Buy'] = np.where((df['Lagging Span'] > df['Close']).shift(26) &
                                 (df['Close'] > df['Conversion Line']) &
                                 (df['Conversion Line'] > df['Base Line']) &
                                 (df['Base Line'] > df['Span A']) &
                                 (df['Base Line'] > df['Span B']),
                                 1, np.NaN)
            # Close Long Position
            df['Buy'] = np.where((df['Close'] < df['Span B']),
                                 0, df['Buy'])
            df['Buy'].ffill(inplace=True)

            # Open Short Position
            df["Sell"] = np.where((df['Lagging Span'] < df['Close']).shift(26) &
                                  (df['Close'] < df['Conversion Line']) &
                                  (df['Conversion Line'] < df['Base Line']) &
                                  (df['Base Line'] < df['Span A']) &
                                  (df['Base Line'] < df['Span B']),
                                  -1, np.NaN)
            # Close Short Position
            df['Sell'] = np.where((df['Close'] > df['Span A']),
                                  0, df['Sell'])
            df['Sell'].ffill(inplace=True)

            df['Position'] = df['Buy'] + df['Sell']
            self._position = df['Position'].copy()
            return self._position
        except Exception:
            return utils.empty_df()

    def recommendation(self):
        """Return the current recommended position

        Returns:
            str: LONG, SHORT or WAIT
        """
        if self._position is None:
            self._strategy()
        try:
            recommendation = self._position.iloc[-1]

            if recommendation > 0:
                return 'LONG'
            elif recommendation < 0:
                return 'SHORT'
            return 'WAIT'
        except Exception:
            pass

    def backtest(self):
        """Compare the asset return with the strategy return
           using historical data

        Returns:
            DataFrame:
                Asset Return: The return if we just holded the asset
                Strategy Return: The return if we'd implemented the strategy
        """
        if self._position is None:
            self._strategy()
        try:
            df = self._history.copy()
            df['Asset Returns'] = np.log(df['Close'] / df['Close'].shift(1))

            df['Strategy Returns'] = df['Asset Returns'] * self._position
            self._backtest_results = df[['Asset Returns',
                                        'Strategy Returns']].copy().cumsum()
            return self._backtest_results
        except Exception:
            return utils.empty_df()


if __name__ == '__main__':
    # You can test any ticker in Yahoo Finance
    ticker = 'AAPL'

    ichimoku = Ichimoku(ticker)

    # You can change the period (it is recommended to be at least 1 year)
    data = ichimoku.history(period='max')

    fig, axes = plt.subplots(1, 2, figsize=(15, 15))
    fig.suptitle(f'{ticker} | Recommendation: {ichimoku.recommendation()}',
                 fontsize=16)
    data.plot(ax=axes[0], title='Ichimoku Graph', logy=True)
    ichimoku.backtest().plot(ax=axes[1], title='Backtested Returns')

    plt.show()
