# Ichimoku Strategy Bot

This project intends to provide a backtested strategy using the Ichimoku Cloud as it's main indicator. **IT SHOULD NOT BE USED AS FINANCIAL ADVICE. PLEASE DO YOUR OWN RESEARCH FIRST**

Check out this [Investopedia post](https://www.investopedia.com/terms/i/ichimoku-cloud.asp) to get information about the Ichimoku Cloud.


## Backtested Results

I've backtested the strategy with a couple of assets. In most cases, it outperformed the market:

![VOD](graphs/VOD.png)
![AAPL](graphs/AAPL.png)
![NFLX](graphs/NFLX.png)
![GS](graphs/GS.png)

It also worked on the crypto market:

![BTC-USD](graphs/BTC-USD.png)
![ETH-USD](graphs/ETH-USD.png)

Of course, there were some assets that the strategy couldn't outperform yet (although it was still profitable)

![GOOGL](graphs/GOOGL.png)
![FB](graphs/FB.png)

I encourage you to backtest the strategy with other assets 

# Quickstart

## The Ichimoku module

```python
from ichimoku import Ichimoku

aapl = Ichimoku('AAPL')

# Get historical data with the Ichimoku indicator values
hist = aapl.history(period='max')

# You can also specify start/end dates
hist = aapl.history(start='2000-01-01', end='2019-12-12')

# Plot the Ichimoku graph with the historical data
hist.plot()

# Backtest the strategy and compare the returns
returns = aapl.backtest()

# Plot the strategy returns and compare it with the market returns
returns.plot()

# Get the current recommended action (LONG, SHORT or WAIT)
recommendation = aapl.recommendation()
```

**Installation**
```console
$ git clone https://bitbucket.org/ferudesa/ichimoku_strategy.git
$ cd ichimoku_strategy
$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

**Requirements**

- [Python](https://www.python.org/) >= 3.6
- [yfinance](https://github.com/ranaroussi/yfinance) >= 0.1.62
- [matplotlib](https://github.com/matplotlib/matplotlib) (tested to work with 3.4.2)

**P.S.** 

Please drop me an note with any feedback you have. I intend to upgrade the strategy.

**Fernando Kricun**